import sys
import time
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from datetime import datetime
from hashlib import sha256
from chain.blockchains import Blockchain
from blocks.block import Block

GOD_HASH = "0x000000e291516e3c947e9172479d12b54c3f3bf54d615cc99721da495fe25d74"

class Miner:
    def __init__(self):
        pass

    def mine_block(self, new_block=None, auto=False):
        start = time.time()
        last_block = Blockchain.get_last()
        if new_block is None:
            new_block = Block(
                    number=last_block.number + 1,
                    parent_block_hash=last_block.merkle_root,
                    txs=[]
                    )
        mined = False
        nonce = 0
        hash = ""
        tx_string = str(new_block.txs)
        print "\n[Mining started on Block number " + str(new_block.number) + "]"
        while mined is False:
            hash = "0x" + sha256(new_block.parent_block_hash + str(nonce) + tx_string).hexdigest()
            sys.stdout.write("Hash: %s; Attempts:  %d; Speed: %sH/s   \r" % (hash, nonce, (nonce + 1)/(time.time() - start)) )
            sys.stdout.flush()
            if "0x0000" in hash[0:6]:
                mined = True
                break
            else:
                nonce += 1
        new_block.nonce = nonce
        new_block.merkle_root = hash
        new_block.time = str(datetime.utcnow())
        Blockchain.insert_block(new_block)
        if auto:
            self.mine_block(auto=True)

    def mine_genesis(self, force=False):
        last_block = Blockchain.get_last()
        if last_block is None or force is True:
            start = time.time()
            genesis = Block(
                    number=0,
                    parent_block_hash=GOD_HASH
                    )
            mined = False
            nonce = 0
            hash = ""
            tx_string = str(genesis.txs)
            print "[Mining started on the Genesis Block]"
            while mined is False:
                hash = "0x" + sha256(genesis.parent_block_hash + str(nonce) + tx_string).hexdigest()
                sys.stdout.write("Hash: %s; Attempts:  %d; Speed: %sH/s   \r" % (hash, nonce, (nonce + 1)/(time.time() - start)) )
                sys.stdout.flush()
                if "0x000000" in hash[0:8]:
                    mined = True
                    break
                else:
                    nonce += 1
            genesis.nonce = nonce
            genesis.merkle_root = hash
            genesis.time = str(datetime.utcnow())
            return Blockchain.insert_genesis(genesis)
        else:
            raise Exception("Genesis block already exists. Genesis block creation must quit...")

