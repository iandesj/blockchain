from chain.blockchains import Blockchain
from miner.miner import Miner
from blocks.block import Block
import pickledb

# God hash consists of
# Nonce = 12271920, Message = "God."
GOD_HASH = "0x000000e291516e3c947e9172479d12b54c3f3bf54d615cc99721da495fe25d74"
db = pickledb.load("blockchain.db", True)
miner = Miner()
genesis = miner.mine_genesis(force=True)
if genesis.nonce is not None:
    print "\n[Genesis Block Created]"

miner.mine_block(auto=True)
