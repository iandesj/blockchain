# -*- coding: utf-8 -*-

from .context import blocks

import unittest


class AdvancedTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_thoughts(self):
        self.assertIsNone(blocks.hmm())


if __name__ == '__main__':
    unittest.main()
