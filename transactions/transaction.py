from hashlib import sha256

class Transaction:
    def __init__(self, block=None, sender=None, receiver=None, data=None):
        self.block = block
        self.sender = sender
        self.receiver = receiver
        self.data = data
        self.id = self.calculate_tx()

    def calculate_tx(self):
        raw_tx = str(self.block) + "|" + self.sender + "|" + self.receiver + "|" + str(self.data)
        return sha256(sha256(raw_tx).hexdigest()).hexdigest()[::-1]
