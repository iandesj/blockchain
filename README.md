Blockchain
========================

Just for fun. Learning the ins and outs.


To run a script that will just start mining, starting with the genesis block, perform:
```bash
bash-$ pip install -r requirements.txt
bash-$ python main.py
```

## TODO
- [x] basic mining of blocks
- [x] genesis block creation
- [ ] p2p capabilities
- [ ] address/accounts on the chain
- [ ] transactions + transactions pool (mempool)
- [ ] transaction validation

..suggestions?