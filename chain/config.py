class Config:
    def __init__(self, reward=None, difficulty=None):
        self.reward = reward
        self.difficulty = difficulty

    @staticmethod
    def default():
        return Config(10, "0x0000")
