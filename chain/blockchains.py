from ast import literal_eval
import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from blocks.block import Block
from config import Config
from hashlib import sha256
import pickledb

# God hash consists of
# Nonce = 12271920, Message = "God."
GOD_HASH = "0x000000e291516e3c947e9172479d12b54c3f3bf54d615cc99721da495fe25d74"
db = pickledb.load("blockchain.db", True)

class Blockchain:
    @staticmethod
    def insert_genesis(genesis=None):
        block_string = str(genesis.__dict__)
        db.set(genesis.merkle_root, block_string)
        db.set("last_block", block_string)
        db.set("index", 0)
        db.dump()
        return genesis

    @staticmethod
    def insert_block(block=None):
        if block is not None:
            block_string = str(block.__dict__)
            db.set(block.merkle_root, block_string)
            db.set("last_block", block_string)
            db.set("index", int(db.get("index")) + 1)
            db.dump()
            return block

    @staticmethod
    def get_last():
        last = db.get("last_block")
        if last is not None:
            return Block(**literal_eval(last))
